#!/usr/bin/env python3
# -*- coding: utf8 -*-

import requests as rq
import json
import psycopg2
from psycopg2.extensions import parse_dsn
import shutil
import os
import pandas as pd
from pandas import DataFrame

try:
    os.remove("./.docker/data/polluant_full.csv")
except FileNotFoundError:
    print('Did not delete file polluant_full.csv that did not exist')
#--------------- Fonctions du script -----------------------------

def download_json_from_url(arg : str):
    print(f'Tentative de téléchargement depuis \"{arg}\"...')
    try:
        data = rq.get(arg).json() # get the json dict if possible
    except:
        data = 0 # else set dummy value
    if type(data) is dict: #Case: got json dict
        if list(data.keys())[0] == 'errors': #Check link integrity
            print(f'Lien invalide: {arg} - returning None')
            print(data['errors'])
            return None #Return none on failed download
        else: #Integrity check passed, returning data
            print('Téléchargement terminé.')
        return data # return the single json dict
    else: #Case: did not get json dict
        print(f'L\'URL \"{arg}\" ne retourne pas un dictionnaire,\n\
            annulation de l\'exécution de la fonction - returning None')
        return None

def extract(*frequences_polluants_et_url):
                         # /!\
    url_checker = 'http' #substring to check against argument to determine whether or not
                         #argument is an URL
    #START ::: List of URLS @ 2020/01/12
    #Format: url_frequence_polluant
    url_horaire_co = 'https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_7.geojson'
    url_horaire_c6h6 = 'https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_6.geojson'
    url_horaire_no = 'https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_0.geojson'
    url_horaire_no2 = 'https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_1.geojson'
    url_horaire_o3 = 'https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_3.geojson'
    url_horaire_pm10 = 'https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_4.geojson'
    url_horaire_pm25 = 'https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_2.geojson'
    url_horaire_so2 = 'https://opendata.arcgis.com/datasets/08264f6574e844b8800ee0e18ca5ff9e_5.geojson'
    url_journalier_co ='https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_8.geojson'
    url_journalier_c6h6 = 'https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_7.geojson'
    url_journalier_no = 'https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_0.geojson'
    url_journalier_nox = 'https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_5.geojson'
    url_journalier_no2 = 'https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_1.geojson'
    url_journalier_o3 = 'https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_2.geojson'
    url_journalier_pm10 = 'https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_4.geojson'
    url_journalier_pm25 = 'https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_3.geojson'
    url_journalier_so2 = 'https://opendata.arcgis.com/datasets/5e828516bbd44aa9aefd1443aac8239f_6.geojson'
    url_mensuel_co = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_8.geojson'
    url_mensuel_c6h6 = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_7.geojson'
    url_mensuel_no = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_0.geojson'
    url_mensuel_nox = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_5.geojson'
    url_mensuel_no2 = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_1.geojson'
    url_mensuel_o3 = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_2.geojson'
    url_mensuel_pm10 = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_4.geojson'
    url_mensuel_pm25 = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_3.geojson'
    url_mensuel_so2 = 'https://opendata.arcgis.com/datasets/a23d338f09d94c76ae5a1e3eac2b8ac6_6.geojson'
    url_2019_co = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_44.geojson'
    url_2019_c6h6 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_39.geojson'
    url_2019_no = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_12.geojson'
    url_2019_nox = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_29.geojson'
    url_2019_no2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_13.geojson'
    url_2019_o3 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_14.geojson'
    url_2019_pm10 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_19.geojson'
    url_2019_pm25 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_24.geojson'
    url_2019_so2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_38.geojson'
    url_2018_co = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_43.geojson'
    url_2018_c6h6 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_33.geojson'
    url_2018_no = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_3.geojson'
    url_2018_nox = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_28.geojson'
    url_2018_no2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_7.geojson'
    url_2018_o3 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_11.geojson'
    url_2018_pm10 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_18.geojson'
    url_2018_pm25 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_23.geojson'
    url_2018_so2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_37.geojson'
    url_2017_co = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_42.geojson'
    url_2017_c6h6 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_32.geojson'
    url_2017_no = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_2.geojson'
    url_2017_nox = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_27.geojson'
    url_2017_no2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_6.geojson'
    url_2017_o3 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_10.geojson'
    url_2017_pm10 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_17.geojson'
    url_2017_pm25 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_22.geojson'
    url_2017_so2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_36.geojson'
    url_2016_co = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_41.geojson'
    url_2016_c6h6 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_31.geojson'
    url_2016_no = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_1.geojson'
    url_2016_nox = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_26.geojson'
    url_2016_no2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_5.geojson'
    url_2016_o3 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_9.geojson'
    url_2016_pm10 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_16.geojson'
    url_2016_pm25 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_21.geojson'
    url_2016_so2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_35.geojson'
    url_2015_co = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_40.geojson'
    url_2015_c6h6 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_30.geojson'
    url_2015_no = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_0.geojson'
    url_2015_nox = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_25.geojson'
    url_2015_no2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_4.geojson'
    url_2015_o3 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_8.geojson'
    url_2015_pm10 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_15.geojson'
    url_2015_pm25 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_20.geojson'
    url_2015_so2 = 'https://opendata.arcgis.com/datasets/0a645653f9a34af49e323678959fc835_34.geojson'
    #END ::: List of URLS @ 2020/01/12

    #Other variables of function
    frequences_polluants_et_url_list = list(frequences_polluants_et_url) #turn tuple of args into list
                                                                    #for more flexible use
    url_choisies = [] # list of all URL arguments
    frequences_choisies = [] # list of all frequency arguments
    polluants_choisis = [] # list of all pollutant arguments
    frequences_existantes = ['horaire', 'journalier', 'mensuel',\
                             '2019', '2018', '2017', '2016', '2015'] # list of all registered frequencies
    polluants_existants = ['co', 'c6h6', 'no', 'nox', 'no2',\
                           'o3', 'pm10', 'pm25', 'so2'] # list of all registered pollutants
    output_list = [] # list to return multiple elements

    #Function Body
    #Default values
    if not frequences_polluants_et_url_list: # if no arguments were given
        frequences_polluants_et_url_list = ['horaire', 'no2', 'pm10'] # use default values as arguments
    
    #Sort arguments
    for item in frequences_polluants_et_url_list: # cycle arguments
        if url_checker in item: # feed URLs list
            url_choisies.append(item)
        elif item in frequences_existantes: # feed frequencies list
            frequences_choisies.append(item)
        elif item in polluants_existants: # feed pollutants list
            polluants_choisis.append(item)
        else:
            pass
    
    #Crash function if invalid argument was given
    for url in url_choisies: # cycle URLs to remove them from arguments list
        frequences_polluants_et_url_list.remove(url)
    for freq in frequences_choisies: # cycle frequencies to remove them from arguments list
        frequences_polluants_et_url_list.remove(freq)
    for poll in polluants_choisis: # cycle pollutants to remove them from arguments list
        frequences_polluants_et_url_list.remove(poll)
    # frequences_polluants_et_url_list is now clear of any valid argument
    if len(frequences_polluants_et_url_list) > 0: # raise error if invalid argument was given
        errors = ''
        for err in frequences_polluants_et_url_list: # prepare error message
            errors += err+' '
        raise AttributeError(str(len(frequences_polluants_et_url_list))+\
                             " invalid argument(s) fed to the function: extract() - - - - : "\
                             +errors) # raise error with message
    #Clear empty variable
    del frequences_polluants_et_url_list

    #Case1: single URL given
    #   return single json_dict not in list
    if len(url_choisies)==1 and not len(frequences_choisies) and not len(polluants_choisis):
        data_single_url = download_json_from_url(url_choisies[0])
        if data_single_url:
            return data_single_url
        else:
            return None
    #Case2: multiple URLs given and/or other arguments given
    #   return list[json_dicts]
    else:
        if len(frequences_choisies):
            if len(polluants_choisis) == 0:
                print('Au moins une fréquence a été choisie sans choisir de polluant.')
                print('Veuillez choisir un polluant.')
                print('Interruption de la fonction extract(). - returning None')
                return None
        if len(polluants_choisis):
            if len(frequences_choisies) == 0:
                print('Au moins un polluant a été choisi sans choisir de polluant.')
                print('La fréquence: \'horaire\' sera choisie par défaut.')
                frequences_choisies.append('horaire')
        if url_choisies:
            if len(url_choisies) == 1:
                print('Une URL a été choisie.')
            else:
                print('Des URL furent choisies. Début des tentatives de téléchargement...')
            for _url in url_choisies:
                custom_download = download_json_from_url(_url)
                if custom_download:
                    output_list.append(custom_download)
        if frequences_choisies:
            for _freq in frequences_choisies:
                for _poll in polluants_choisis:
                    if _freq=='horaire' and _poll=='nox':
                        print('--------------------------------')
                        print('No data available @horaire, nox.')
                        print('No data available @horaire, nox.')
                        print('--------------------------------')
                    else:
                        info = f'Téléchargement du dataset en format json dict: mes aura {_freq} {_poll} en cours...'
                        print(info)
                        output_list.append(rq.get(locals()[f'url_{_freq}_{_poll}']).json())
                        print('Téléchargement terminé')
        if len(output_list) == 1:
            return output_list[0]
        else:
            return output_list

def json_to_csv(datas):
    if type(datas) is not list:
        datas = [datas]
    raw_data_full = {"nom_dept":[], "nom_com":[], "insee_com":[], "nom_station":[],\
                "code_station":[], "typologie":[], "influence":[], "nom_poll":[], "id_poll_ue":[],\
                "valeur":[], "unite":[], "metrique":[], "date_debut":[], "date_fin":[],\
                "x_wgs84":[], "y_wgs84":[],"OBJECTID":[]}
    keys = ['nom_dept', 'nom_com', 'insee_com', 'nom_station',\
            'code_station', 'typologie', 'influence', 'nom_poll', 'id_poll_ue',\
            'valeur', 'unite', 'metrique', 'date_debut', 'date_fin',\
            'x_wgs84', 'y_wgs84', 'OBJECTID', 'id_com', 'xl93', 'yl93']

    iii = 0
    kkk = 0
    for dict_json in datas:
        for elem in dict_json.get('features'):
            iii += 1
            for k in keys:
                if (k=='id_com') and (k in elem.get('properties').keys()):
                    raw_data_full['insee_com'].append(elem.get('properties').get(k))
                elif (k=='xl93') and (k in elem.get('properties').keys()):
                    raw_data_full['x_wgs84'].append(elem.get('properties').get(k))
                elif (k=='yl93') and (k in elem.get('properties').keys()):
                    raw_data_full['y_wgs84'].append(elem.get('properties').get(k))
                elif k in elem.get('properties').keys():
                    raw_data_full[k].append(elem.get('properties').get(k))
                else:
                    if not iii % 10000:
                        print(iii, '< lines -:-', kkk, '< keys -:- index of data_list >', datas.index(dict_json))
    
    df = DataFrame(raw_data_full)
    df.to_csv("polluant_full.csv", index=False)

def sql_tables():
    
    try:
        shutil.move("polluant_full.csv", "./.docker/data/")
    except:
        print('Error at shutil.move around line 249 in sql_tables() function')
    db_dsn = "postgres://postgres:test@localhost:5432/projet2"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)

    cur = conn.cursor()

    cur.execute("""CREATE TABLE IF NOT EXISTS raw_data_full(
        "nom_dept" TEXT,
        "nom_com" TEXT,
        "insee_com" INT,
        "nom_station" TEXT,
        "code_station" TEXT,
        "typologie" TEXT,
        "influence" TEXT,
        "nom_poll" TEXT,
        "id_poll_ue" INT,
        "valeur" FLOAT,
        "unite" TEXT,
        "metrique" TEXT,    
        "date_debut" TIMESTAMP WITH TIME ZONE,
        "date_fin" TIMESTAMP WITH TIME ZONE,
        "longitude" FLOAT,
        "lattitude" FLOAT,
        "OBJECTID" INT);""")

    cur.execute("DELETE FROM raw_data_full")
    cur.execute("COPY raw_data_full FROM '/data/polluant_full.csv' (FORMAT 'csv', DELIMITER ',', HEADER TRUE);")

    cur.execute("""CREATE TABLE IF NOT EXISTS raw_data_full_history(
        "nom_dept" TEXT,
        "nom_com" TEXT,
        "insee_com" INT,
        "nom_station" TEXT,
        "code_station" TEXT,
        "typologie" TEXT,
        "influence" TEXT,
        "nom_poll" TEXT,
        "id_poll_ue" INT,
        "valeur" FLOAT,
        "unite" TEXT,
        "metrique" TEXT,    
        "date_debut" TIMESTAMP WITH TIME ZONE,
        "date_fin" TIMESTAMP WITH TIME ZONE,
        "longitude" FLOAT,
        "lattitude" FLOAT,
        "OBJECTID" INT);""")
    conn.commit()

    cur.execute("""INSERT INTO raw_data_full_history SELECT * FROM raw_data_full WHERE (raw_data_full.valeur IS NOT NULL)
                    AND NOT (raw_data_full.valeur = 0)
                    AND NOT EXISTS (   SELECT * FROM raw_data_full_history
                    WHERE ((raw_data_full_history.nom_station, raw_data_full_history.nom_poll, raw_data_full_history.date_debut, raw_data_full_history.metrique, raw_data_full_history.valeur)
                    = (raw_data_full.nom_station, raw_data_full.nom_poll, raw_data_full.date_debut, raw_data_full.metrique, raw_data_full.valeur))   )
                """)
    
    cur.execute("""DROP TABLE IF EXISTS departement_full CASCADE""")
    cur.execute("""CREATE TABLE IF NOT EXISTS departement_full (
        "id_departement" SERIAL,
        "nom_dept" TEXT,
        PRIMARY KEY ("id_departement"));""")

    cur.execute("""DROP TABLE IF EXISTS commune_full CASCADE""")
    cur.execute("""CREATE TABLE IF NOT EXISTS commune_full (
        "id_dept" INT, 
        "nom_com" TEXT,
        "insee_com" INT,
        PRIMARY KEY ("insee_com"),
        FOREIGN KEY ("id_dept") REFERENCES departement_full ("id_departement"));""")

    cur.execute("""DROP TABLE IF EXISTS station_full CASCADE""")
    cur.execute("""CREATE TABLE IF NOT EXISTS station_full (
        "id_station" SERIAL,
        "code_station" TEXT,
        "id_com" INT,
        "nom_station" TEXT,
        "longitude" FLOAT,
        "lattitude" FLOAT,
        PRIMARY KEY ("id_station"),
        FOREIGN KEY ("id_com") REFERENCES commune_full ("insee_com"));""")
    
    cur.execute("""DROP TABLE IF EXISTS polluant_full CASCADE""")
    cur.execute("""CREATE TABLE IF NOT EXISTS polluant_full (
        "id_polluant" SERIAL,
        "nom_poll" TEXT,
        PRIMARY KEY ("id_polluant"));""")
    
    cur.execute("""DROP TABLE IF EXISTS releve_full CASCADE""")
    cur.execute("""CREATE TABLE IF NOT EXISTS releve_full (
        "id_releve" SERIAL,
        "id_pol_releve" INT, 
        "id_station" INT,
        "date_heure_releve" TIMESTAMP WITH TIME ZONE,
        "metrique" TEXT,
        "valeur" FLOAT,
        "unite" TEXT,
        PRIMARY KEY ("id_releve"),
        FOREIGN KEY ("id_pol_releve") REFERENCES polluant_full ("id_polluant"),
        FOREIGN KEY ("id_station") REFERENCES station_full ("id_station"));""")

    conn.commit()
    
    cur.close()
    conn.close()

def load_sql_tables():
    db_dsn = "postgres://postgres:test@localhost:5432/projet2"
    db_args = parse_dsn(db_dsn)
    conn = psycopg2.connect(**db_args)

    cur = conn.cursor()

    cur.execute("DELETE FROM releve_full;")
    cur.execute("DELETE FROM station_full;")
    cur.execute("DELETE FROM commune_full;")
    cur.execute("DELETE FROM departement_full;")
    cur.execute("DELETE FROM polluant_full;")

    cur.execute("""INSERT INTO departement_full ("nom_dept")
    SELECT DISTINCT(nom_dept)
    FROM raw_data_full""")

    conn.commit()

    cur.execute("""INSERT INTO commune_full ("nom_com", "insee_com", "id_dept") 
    SELECT DISTINCT raw_data_full.nom_com, raw_data_full.insee_com, departement_full.id_departement 
    FROM raw_data_full
    JOIN departement_full
    ON departement_full.nom_dept = raw_data_full.nom_dept; """)

    conn.commit()
    

    cur.execute("""INSERT INTO station_full ("nom_station", "longitude", "lattitude", "code_station", "id_com") 
    SELECT DISTINCT(raw_data_full.nom_station), raw_data_full.longitude, raw_data_full.lattitude, raw_data_full.code_station, commune_full.insee_com 
    FROM raw_data_full 
    JOIN commune_full
    ON commune_full.insee_com = raw_data_full.insee_com; """)

    conn.commit()
    

    cur.execute("""INSERT INTO polluant_full("nom_poll") 
    SELECT DISTINCT(nom_poll)
    FROM raw_data_full;""")

    conn.commit()


    cur.execute("""INSERT INTO releve_full ("id_pol_releve", "id_station", "date_heure_releve", "metrique", "valeur", "unite") 
    SELECT polluant_full.id_polluant, station_full.id_station, raw_data_full.date_debut, raw_data_full.metrique, raw_data_full.valeur, raw_data_full.unite
    FROM raw_data_full
    JOIN polluant_full
    ON polluant_full.nom_poll = raw_data_full.nom_poll
    JOIN station_full
    ON  station_full.nom_station = raw_data_full.nom_station;""")

    conn.commit()
    
    cur.close()
    conn.close()
#-----------------------------------------------------------------
#--------------- Convertion du fichier Json en CSV --------------- 

json_to_csv(extract('horaire', 'journalier', 'mensuel', '2019', '2018', '2017', '2016', '2015',\
                        'co', 'c6h6', 'no', 'nox', 'no2', 'o3', 'pm10', 'pm25', 'so2'))

#----------------------------------------------------------------- 
#--------------- Creation des table ------------------------------

sql_tables()

#-----------------------------------------------------------------
#-------------- Allimentation des tables -------------------------

load_sql_tables()

#-----------------------------------------------------------------

# os.remove("./.docker/data/polluant_full.csv")

